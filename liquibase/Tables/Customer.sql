--liquibase formatted sql

--changeSet PeHe:AddCustomer endDelimiter:\nGO splitStatements:true stripComments:false runOnChange:false

Create table Customer
(
	CusomerId int identity(1, 1) constraint [PK_Customer] primary key,
	Name varchar(500),
	GLN  bigint
);