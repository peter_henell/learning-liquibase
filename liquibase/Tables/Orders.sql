--liquibase formatted sql

--changeSet PeHe:AddingORdersTable endDelimiter:\nGO splitStatements:true stripComments:false runOnChange:false

Create table Orders
(
	OrderId int identity(1, 1) constraint [PK_Order] primary key,
	CusomerId int foreign key references Customer(CusomerId),
	Name varchar(500),
	Amount decimal
);