--liquibase formatted sql

--changeSet PeHe:AddFancyReport endDelimiter:\nGO splitStatements:true stripComments:false runOnChange:True
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

ALTER PROCEDURE [dbo].[FancyReport] 
	@reportDate DATE = NULL
AS
begin
	select DATEADD(day, -5, @reportDate);
end
